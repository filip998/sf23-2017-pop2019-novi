﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;

namespace SF23_2017_POP2019Novi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnPrijava_Click(object sender, RoutedEventArgs e)
        {
            SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-NM9C3CP\FILIPSQL;Initial Catalog=POP2019;Integrated Security=True");

            SqlDataAdapter sda1 = new SqlDataAdapter("select TipKorisnika from Korisnik where KorisnickoIme='" + tbKorisnickoIme.Text + "' and Lozinka='" + tbLozinka.Text + "'", con);
            DataTable dt1 = new DataTable();
            sda1.Fill(dt1);
            if (dt1.Rows.Count == 1)
            {
                switch (dt1.Rows[0]["TipKorisnika"] as string)
                {
                    case "Admin":
                        {
                            this.Hide();
                            new Admin().Show();
                            break;
                        }
                    case "Profesor":
                        {
                            this.Hide();
                            new Profesor(tbKorisnickoIme.Text).Show();
                            break;
                        }
                    case "Asistent":
                        {
                            this.Hide();
                            new Asistent(tbKorisnickoIme.Text).Show();
                            break;
                        }
                    default:
                        {
                            MessageBox.Show("Unesite korisniko ime i lozinku", "Neuspesno", MessageBoxButton.OK, MessageBoxImage.Error);
                            break;
                        }
                }
                tbKorisnickoIme.Text = "";
                tbLozinka.Text = "";
            }
        }

        private void BtnIzlaz_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnNeregistrovanKorinsik_Click(object sender, RoutedEventArgs e)
        {
            NeregistrovanKorisnik frm = new NeregistrovanKorisnik();
            frm.ShowDialog();
        }
    }
}
