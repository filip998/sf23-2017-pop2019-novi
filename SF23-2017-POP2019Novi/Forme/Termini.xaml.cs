﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using SF23_2017_POP2019Novi.Model;

namespace SF23_2017_POP2019Novi
{
    /// <summary>
    /// Interaction logic for Termini.xaml
    /// </summary>
    public partial class Termini : Window
    {
        public Termini()
        {
            InitializeComponent();
            UcitajTermine();
            popuniComboBox();
            

        }
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-NM9C3CP\FILIPSQL;Initial Catalog=POP2019;Integrated Security=True");
        public int TerminId { get; set; }
        private void popuniComboBox()
        {
            List<String> tipoviNastave = new List<String>() { "Vezbe", "Predavanja" };
            cbTipNastave.ItemsSource = tipoviNastave;
            List<String>daniUNedelji = new List<string>() { "Ponedeljak", "Utorak", "Sreda", "Cetvrtak", "Petak", "Subota", "Nedelja" };
            cbDaniUNedelji.ItemsSource = daniUNedelji;
        }


        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            if (isValid())
            {
                SqlCommand cmd = new SqlCommand("INSERT INTO Termini VALUES(@SifraTermina,@VremeZauzeca,@DaniNedelji,@TipNastave,@Korisnik,@UcionicaID)", con);
                cmd.CommandType = CommandType.Text;


                cmd.Parameters.AddWithValue("@SifraTermina", tbSifraTermina.Text);
                cmd.Parameters.AddWithValue("@VremeZauzeca", tbVremeZauzeca.Text);
                cmd.Parameters.AddWithValue("@DaniNedelji", cbDaniUNedelji.Text);
                cmd.Parameters.AddWithValue("@TipNastave", cbTipNastave.Text);
                cmd.Parameters.AddWithValue("@Korisnik", tbKorisnik.Text);
                cmd.Parameters.AddWithValue("@UcionicaID", 2);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                MessageBox.Show("Novi termin je dodat!", "Sacuvano", MessageBoxButton.OK, MessageBoxImage.Information);
                UcitajTermine();
                Reset();
            }
        }

        private void Reset()
        {
            TerminId = 0;
            tbSifraTermina.Clear();
            tbVremeZauzeca.Clear();
            cbDaniUNedelji.SelectedIndex = -1;
            cbTipNastave.SelectedIndex = -1;
            tbKorisnik.Clear();
        }

        private void UcitajTermine()
        {
            SqlCommand cmd = new SqlCommand("Select * from Termini", con);
            DataTable dt = new DataTable();

            con.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            dt.Load(sdr);
            con.Close();

            DGTermini.ItemsSource = dt.DefaultView;
        }

        private bool isValid()
        {
            if (tbSifraTermina.Text == string.Empty)
            {
                MessageBox.Show("Sifra termina je neophodna", "Neuspesno", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;

            }
            else if (tbVremeZauzeca.Text == string.Empty)
            {
                MessageBox.Show("Vreme zauzeca je neophodno", "Neuspesno", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else if (cbDaniUNedelji.Text == string.Empty)
            {
                MessageBox.Show("Dani u nedelji su neophodni", "Neuspesno", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else if (cbTipNastave.Text == string.Empty)
            {
                MessageBox.Show("Tip nastave je neophodan", "Neuspesno", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }

            return true;
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            if (TerminId > 0)
            {
                SqlCommand cmd = new SqlCommand("UPDATE  Termini SET SifraTermina=@SifraTermina,VremeZauzeca=@VremeZauzeca,DaniNedelji=@DaniNedelji,TipNastave=@TipNastave,Korisnik=@Korisnik WHERE Id=@ID", con);
                cmd.CommandType = CommandType.Text;


                cmd.Parameters.AddWithValue("@SifraTermina", tbSifraTermina.Text);
                cmd.Parameters.AddWithValue("@VremeZauzeca", tbVremeZauzeca.Text);
                cmd.Parameters.AddWithValue("@DaniNedelji", cbDaniUNedelji.Text);
                cmd.Parameters.AddWithValue("@TipNastave", cbTipNastave.Text);
                cmd.Parameters.AddWithValue("@Korisnik", tbKorisnik.Text);

                cmd.Parameters.AddWithValue("@ID", this.TerminId);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                MessageBox.Show("Termin uspesno izmenjen!", "Izmenjeno", MessageBoxButton.OK, MessageBoxImage.Information);
                UcitajTermine();
                Reset();

            }
            else
            {
                MessageBox.Show("Izaberite termin za izmenu!", "Izaberite", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            if (TerminId > 0)
            {
                SqlCommand cmd = new SqlCommand("DELETE FROM Termini WHERE Id = @ID", con);
                cmd.CommandType = CommandType.Text;


                cmd.Parameters.AddWithValue("@ID", this.TerminId);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                MessageBox.Show("Termin uspesno izbrisan!", "Izbrisano", MessageBoxButton.OK, MessageBoxImage.Information);
                UcitajTermine();
                Reset();

            }
            else
            {
                MessageBox.Show("Izaberite termin za brisanje!", "Izaberite", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void DGTermini_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            DataGrid dg = sender as DataGrid;
            DataRowView dr = dg.SelectedItem as DataRowView;
            if (dr != null)
            {
                TerminId = Convert.ToInt32(dr["Id"]);
                tbSifraTermina.Text = dr["SifraTermina"].ToString();
                tbKorisnik.Text = dr["Korisnik"].ToString();
                tbVremeZauzeca.Text = dr["VremeZauzeca"].ToString();
                cbDaniUNedelji.Text = dr["DaniNedelji"].ToString();
                cbTipNastave.Text = dr["TipNastave"].ToString();
                
            }
        }

        private void BtnPretraga_Click(object sender, RoutedEventArgs e)
        {
            SqlCommand cmd1 = new SqlCommand("Select * from Termini Where SifraTermina='" + tbPretraga.Text + "'OR VremeZauzeca='" + tbPretraga.Text + "'OR DaniNedelji='" + tbPretraga.Text + "'OR TipNastave='" + tbPretraga.Text + "'OR Korisnik='" + tbPretraga.Text + "'", con);
            DataTable dt1 = new DataTable();

            con.Open();
            SqlDataReader sda = cmd1.ExecuteReader();
            dt1.Load(sda);
            con.Close();

            DGTermini.ItemsSource = dt1.DefaultView;
        }
    }
}
