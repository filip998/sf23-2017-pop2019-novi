﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;


namespace SF23_2017_POP2019Novi
{
    /// <summary>
    /// Interaction logic for Profesor.xaml
    /// </summary>
    public partial class Profesor : Window
    {
        
        public Profesor(string imeProfesora)
        {

            InitializeComponent();
            ucitajKorisnike(imeProfesora);
            IspisiPodatke(imeProfesora);
           
        }
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-NM9C3CP\FILIPSQL;Initial Catalog=POP2019;Integrated Security=True");
        private void ucitajKorisnike(string imeProf)
        {
            SqlCommand cmd = new SqlCommand("select a.Ime,a.Prezime,a.Email from Korisnik a,Korisnik b where a.Id = b.DrugiKorisnikId and b.KorisnickoIme='" + imeProf + "'", con);
            DataTable dt = new DataTable();

            con.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            dt.Load(sdr);
            con.Close();

            DGAsistenti.ItemsSource = dt.DefaultView;
        }

        private void IspisiPodatke(string imeProfesora)
        {
            SqlCommand cmd = new SqlCommand("select Ime,Prezime,Email,TipKorisnika from Korisnik where KorisnickoIme='" + imeProfesora + "'", con);
            con.Open();
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    tbIme.Text = reader["Ime"].ToString();
                    tbPrezime.Text = reader["Prezime"].ToString();
                    tbEmail.Text = reader["Email"].ToString();
                    tbUloga.Text = reader["TipKorisnika"].ToString();

                }
            }
            con.Close();
        }

    }
}
