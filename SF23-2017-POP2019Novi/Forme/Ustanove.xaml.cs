﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;


namespace SF23_2017_POP2019Novi
{
    /// <summary>
    /// Interaction logic for Ustanove.xaml
    /// </summary>
    public partial class Ustanove : Window
    {
        public Ustanove()
        {
            InitializeComponent();
            UcitajUstanove();
        }

        private void UcitajUstanove()
        {

            SqlCommand cmd = new SqlCommand("Select * from Ustanove", con);
            DataTable dt = new DataTable();

            con.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            dt.Load(sdr);
            con.Close();

            DGUstanove.ItemsSource = dt.DefaultView;
        }

        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-NM9C3CP\FILIPSQL;Initial Catalog=POP2019;Integrated Security=True");
        public int UstanovaId { get; set; }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
           
        }

        private void Reset()
        {
            tbSifraUstanove.Clear();
            tbNaziv.Clear();
            tbAdresa.Clear();
        }

        private bool isValid()
        {
            if (tbNaziv.Text == string.Empty)
            {
                MessageBox.Show("Ime Ustanove je neophodno", "Neuspesno", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;

            }
            else if (tbSifraUstanove.Text == string.Empty)
            {
                MessageBox.Show("Sifra ustanove je neophodna", "Neuspesno", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else if (tbAdresa.Text == string.Empty)
            {
                MessageBox.Show("Adresa ustanove je neophodna", "Neuspesno", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            return true;
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            if (UstanovaId > 0)
            {
                SqlCommand cmd = new SqlCommand("UPDATE  Ustanove SET SifraUstanove=@SifraUstanove,Naziv=@Naziv,Adresa=@Adresa WHERE Id=@ID", con);
                cmd.CommandType = CommandType.Text;


                cmd.Parameters.AddWithValue("@SifraUstanove", tbSifraUstanove.Text);
                cmd.Parameters.AddWithValue("@Naziv", tbNaziv.Text);
                cmd.Parameters.AddWithValue("@Adresa", tbAdresa.Text);
                cmd.Parameters.AddWithValue("@ID", this.UstanovaId);


                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                MessageBox.Show("Ustanova uspesno izmenjena!", "Izmenjeno", MessageBoxButton.OK, MessageBoxImage.Information);
                UcitajUstanove();
                Reset();

            }
            else
            {
                MessageBox.Show("Izaberite ustanovu za izmenu!", "Izaberite", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            if (UstanovaId > 0)
            {
                SqlCommand cmd = new SqlCommand("DELETE FROM Ustanove WHERE Id = @ID", con);
                cmd.CommandType = CommandType.Text;


                cmd.Parameters.AddWithValue("@ID", this.UstanovaId);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                MessageBox.Show("Ustanova uspesno izbrisana!", "Izbrisano", MessageBoxButton.OK, MessageBoxImage.Information);
                UcitajUstanove();
                Reset();


            }
            else
            {
                MessageBox.Show("Izaberite ustnovu za brisanje!", "Izaberite", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (isValid())
            {
                SqlCommand cmd = new SqlCommand("INSERT INTO Ustanove VALUES(@SifraUstanove,@Naziv,@Adresa)", con);
                cmd.CommandType = CommandType.Text;


                cmd.Parameters.AddWithValue("@SifraUstanove", tbSifraUstanove.Text);
                cmd.Parameters.AddWithValue("@Naziv", tbNaziv.Text);
                cmd.Parameters.AddWithValue("@Adresa", tbAdresa.Text);


                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                MessageBox.Show("Nova ustanova je dodata!", "Sacuvano", MessageBoxButton.OK, MessageBoxImage.Information);
                UcitajUstanove();
                Reset();
            }
        }

        private void DGUstanove_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            DataRowView dr = dg.SelectedItem as DataRowView;
            if (dr != null)
            {
                UstanovaId = Convert.ToInt32(dr["Id"]);
                tbSifraUstanove.Text = dr["SifraUstanove"].ToString();
                tbNaziv.Text = dr["Naziv"].ToString();
                tbAdresa.Text = dr["Adresa"].ToString();
                

            }
        }

        private void DGUstanove_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {

            
        }

        private void BtnPretraga_Click(object sender, RoutedEventArgs e)
        {
            SqlCommand cmd1 = new SqlCommand("Select * from Ustanove Where SifraUstanove='" + tbPretraga.Text + "'OR Naziv='" + tbPretraga.Text + "'OR Adresa='" + tbPretraga.Text + "'", con);
            DataTable dt1 = new DataTable();

            con.Open();
            SqlDataReader sda = cmd1.ExecuteReader();
            dt1.Load(sda);
            con.Close();

            DGUstanove.ItemsSource = dt1.DefaultView;
        }
    }
}
