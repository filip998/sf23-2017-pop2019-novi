﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SF23_2017_POP2019Novi
{
    /// <summary>
    /// Interaction logic for Admin.xaml
    /// </summary>
    public partial class Admin : Window
    {
        public Admin()
        {
            InitializeComponent();
        }

        private void BtnKorisnici_Click(object sender, RoutedEventArgs e)
        {
            Korisnici frm = new Korisnici();
            frm.ShowDialog();
        }

        private void BtnUcionice_Click(object sender, RoutedEventArgs e)
        {
            Ucionice frm = new Ucionice();
            frm.ShowDialog();
        }

        private void BtnUstanove_Click(object sender, RoutedEventArgs e)
        {
            Ustanove frm = new Ustanove();
            frm.ShowDialog();
        }

        private void BtnTermini_Click(object sender, RoutedEventArgs e)
        {
            Termini frm = new Termini();
            frm.ShowDialog();
        }
    }
}
