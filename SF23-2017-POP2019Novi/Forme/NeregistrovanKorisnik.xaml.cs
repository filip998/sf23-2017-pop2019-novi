﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;

namespace SF23_2017_POP2019Novi
{
    /// <summary>
    /// Interaction logic for NeregistrovanKorisnik.xaml
    /// </summary>
    public partial class NeregistrovanKorisnik : Window
    {
        public NeregistrovanKorisnik()
        {
            InitializeComponent();
            ucitajKorisnike();
            ucitajUstanove();
            ucitajRaspored();
        }
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-NM9C3CP\FILIPSQL;Initial Catalog=POP2019;Integrated Security=True");
        private void ucitajKorisnike()
        {
            SqlCommand cmd = new SqlCommand("select Ime,Prezime,Email,TipKorisnika,Naziv from Korisnik,Ustanove where UstanovaId = Ustanove.Id", con);
            DataTable dt = new DataTable();

            con.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            dt.Load(sdr);
            con.Close();

            DGZaposlenaLica.ItemsSource = dt.DefaultView;
        }

        private void ucitajUstanove()
        {
            SqlCommand cmd = new SqlCommand("Select SifraUstanove,Naziv,Adresa from Ustanove", con);
            DataTable dt = new DataTable();

            con.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            dt.Load(sdr);
            con.Close();

            DGUstanove.ItemsSource = dt.DefaultView;
        }
        private void ucitajRaspored()
        {
            SqlCommand cmd = new SqlCommand("select TipNastave,DaniNedelji,VremeZauzeca,BrojUcionice,BrojMesta,Ime,Prezime,TipKorisnika from Termini,Ucionice,Korisnik where Termini.UcionicaID = Ucionice.Id and Termini.Korisnik = Korisnik.Id", con);
            DataTable dt = new DataTable();

            con.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            dt.Load(sdr);
            con.Close();

            DGRaspored.ItemsSource = dt.DefaultView;
        }


    }

}
