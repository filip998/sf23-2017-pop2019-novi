﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;

namespace SF23_2017_POP2019Novi
{
    /// <summary>
    /// Interaction logic for Ucionice.xaml
    /// </summary>
    public partial class Ucionice : Window
    {
        public Ucionice()
        {
            InitializeComponent();
            UcitajUcionice();
        }
        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-NM9C3CP\FILIPSQL;Initial Catalog=POP2019;Integrated Security=True");
        public int UcionicaId { get; set; }
        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            if (isValid())
            {
                    SqlCommand cmd = new SqlCommand("INSERT INTO Ucionice VALUES(@SifraUcionice,@BrojUcionice,@BrojMesta,@TipUcionice,@UstanovaId)", con);
                    cmd.CommandType = CommandType.Text;


                    cmd.Parameters.AddWithValue("@SifraUcionice", tbSifraUcionice.Text);
                    cmd.Parameters.AddWithValue("@BrojUcionice", tbBrojUcionice.Text);
                    cmd.Parameters.AddWithValue("@BrojMesta", tbBrojMesta.Text);
                    cmd.Parameters.AddWithValue("@TipUcionice", cbTipUcionice.Text);
                    cmd.Parameters.AddWithValue("@UstanovaId",1);

                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();

                    MessageBox.Show("Nova ucionice je dodata!", "Sacuvano", MessageBoxButton.OK, MessageBoxImage.Information);
                    UcitajUcionice();
                    Reset();
                
            }
        }

        private void UcitajUcionice()
        {
            SqlCommand cmd = new SqlCommand("Select * from Ucionice", con);
            DataTable dt = new DataTable();

            con.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            dt.Load(sdr);
            con.Close();

            DGUcionice.ItemsSource = dt.DefaultView;
        }

        private bool isValid()
        {
            if (tbSifraUcionice.Text == string.Empty)
            {
                MessageBox.Show("Sifra ucionice je neophodna", "Neuspesno", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;

            }
            else if (tbBrojUcionice.Text == string.Empty)
            {
                MessageBox.Show("Broj ucionice je neophodan", "Neuspesno", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else if (tbBrojMesta.Text == string.Empty)
            {
                MessageBox.Show("Broj mesta je neophodno navesti", "Neuspesno", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            return true;
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            if(UcionicaId>0)
            {
                SqlCommand cmd = new SqlCommand("UPDATE  Ucionice SET SifraUcionice=@SifraUcionice,BrojUcionice=@BrojUcionice,BrojMesta=@BrojMesta,TipUcionice=@TipUcionice WHERE Id=@ID", con);
                cmd.CommandType = CommandType.Text;


                cmd.Parameters.AddWithValue("@SifraUcionice", tbSifraUcionice.Text);
                cmd.Parameters.AddWithValue("@BrojUcionice", tbBrojUcionice.Text);
                cmd.Parameters.AddWithValue("@BrojMesta", tbBrojMesta.Text);

                cmd.Parameters.AddWithValue("@TipUcionice", cbTipUcionice.Text);
                cmd.Parameters.AddWithValue("@ID", this.UcionicaId);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                MessageBox.Show("Ucionica uspesno izmenjena!", "Izmenjeno", MessageBoxButton.OK, MessageBoxImage.Information);
                UcitajUcionice();
                Reset();
            }
            else
            {
                MessageBox.Show("Izaberite ucionicu za izmenu!", "Izaberite", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            if(UcionicaId>0)
            {
                SqlCommand cmd = new SqlCommand("DELETE FROM Ucionice WHERE Id = @ID", con);
                cmd.CommandType = CommandType.Text;


                cmd.Parameters.AddWithValue("@ID", this.UcionicaId);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                MessageBox.Show("Ucionica uspesno izbrisana!", "Izbrisano", MessageBoxButton.OK, MessageBoxImage.Information);
                UcitajUcionice();
                Reset();


            }
            else
            {
                MessageBox.Show("Izaberite ucionicu za brisanje!", "Izaberite", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }


        private void Reset()
        {

            UcionicaId = 0;
            tbSifraUcionice.Clear();
            tbBrojUcionice.Clear();
            tbBrojMesta.Clear();
            
            cbTipUcionice.SelectedIndex = -1;

        }


        private void DGUcionice_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
           
        }

        private void DGUcionice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            DataRowView dr = dg.SelectedItem as DataRowView;
            if (dr != null)
            {
                UcionicaId = Convert.ToInt32(dr["Id"]);
                tbSifraUcionice.Text = dr["SifraUcionice"].ToString();
                tbBrojUcionice.Text = dr["BrojUcionice"].ToString();
                tbBrojMesta.Text = dr["BrojMesta"].ToString();
                cbTipUcionice.Text = dr["TipUcionice"].ToString();

            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            SqlCommand cmd = new SqlCommand("Select * from Ucionice Where SifraUcionice='" + tbPretraga.Text + "'OR BrojUcionice='" + tbPretraga.Text + "'OR BrojMesta='" + tbPretraga.Text + "'OR TipUcionice='" + tbPretraga.Text + "'", con);
            DataTable dt = new DataTable();

            con.Open();
            SqlDataReader sda = cmd.ExecuteReader();
            dt.Load(sda);
            con.Close();

            DGUcionice.ItemsSource = dt.DefaultView;
        }
    }
}
