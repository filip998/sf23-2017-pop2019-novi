﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;




namespace SF23_2017_POP2019Novi
{
    /// <summary>
    /// Interaction logic for Korisnici.xaml
    /// </summary>
    public partial class Korisnici : Window
    {
        
        public Korisnici()
        {
            InitializeComponent();
            FillDataGrid();
            popuniComboBox();
        }

        private void FillDataGrid()
        {
            UcitajKorisnike();
            
        }

        private void popuniComboBox()
        {
            List<String> tipoviKorisnika = new List<String>() { "profesor", "asistent"};
            cbTipKorisnika.ItemsSource = tipoviKorisnika;
        }

        SqlConnection con = new SqlConnection(@"Data Source=DESKTOP-NM9C3CP\FILIPSQL;Initial Catalog=POP2019;Integrated Security=True");
        public int KorisnikId { get; set; }

        private bool isValid()
        {
            if (tbIme.Text == string.Empty)
            {
                MessageBox.Show("Ime korisnika je neophodno", "Neuspesno", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;

            }
            else if (tbPrezime.Text == string.Empty)
            {
                MessageBox.Show("Prezime je neophodno", "Neuspesno", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else if (tbKorisnickoIme.Text == string.Empty)
            {
                MessageBox.Show("Korisnicko ime je neophodno", "Neuspesno", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else if (tbLozinka.Text == string.Empty)
            {
                MessageBox.Show("Lozinka je neophodna", "Neuspesno", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            else if (tbEmail.Text == string.Empty)
            {
                MessageBox.Show("Email je neophodan", "Neuspesno", MessageBoxButton.OK, MessageBoxImage.Error);
                return false;
            }
            return true;
        }


        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            if (isValid())
            {
                SqlCommand cmd = new SqlCommand("INSERT INTO Korisnik VALUES(@Ime,@Prezime,@Email,@KorisnickoIme,@Lozinka,@TipKorisnika,@UstanovaId,@DrugiKorisnikId)", con);
                cmd.CommandType = CommandType.Text;


                cmd.Parameters.AddWithValue("@Ime", tbIme.Text);
                cmd.Parameters.AddWithValue("@Prezime", tbPrezime.Text);
                cmd.Parameters.AddWithValue("@Email", tbEmail.Text);
                cmd.Parameters.AddWithValue("@KorisnickoIme", tbKorisnickoIme.Text);
                cmd.Parameters.AddWithValue("@Lozinka", tbLozinka.Text);
                cmd.Parameters.AddWithValue("@TipKorisnika", cbTipKorisnika.Text);
                cmd.Parameters.AddWithValue("@UstanovaId", -1);
                cmd.Parameters.AddWithValue("@DrugiKorisnikId",-1);


                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                MessageBox.Show("Novi korisnik je dodat!", "Sacuvano", MessageBoxButton.OK, MessageBoxImage.Information);
                UcitajKorisnike();
                Reset();
            }
        }
        private void Reset()
        {
            KorisnikId = 0;
            tbIme.Clear();
            tbPrezime.Clear();
            tbEmail.Clear();
            tbKorisnickoIme.Clear();
            tbLozinka.Clear();
            cbTipKorisnika.SelectedIndex = -1;
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            if(KorisnikId>0)
            {
                SqlCommand cmd = new SqlCommand("UPDATE  Korisnik SET Ime=@Ime,Prezime=@Prezime,Email=@Email,KorisnickoIme=@KorisnickoIme,Lozinka=@Lozinka,TipKorisnika=@TipKorisnika WHERE Id=@ID", con);
                cmd.CommandType = CommandType.Text;


                cmd.Parameters.AddWithValue("@Ime", tbIme.Text);
                cmd.Parameters.AddWithValue("@Prezime", tbPrezime.Text);
                cmd.Parameters.AddWithValue("@Email", tbEmail.Text);
                cmd.Parameters.AddWithValue("@KorisnickoIme", tbKorisnickoIme.Text);
                cmd.Parameters.AddWithValue("@Lozinka", tbLozinka.Text);
                cmd.Parameters.AddWithValue("@TipKorisnika", cbTipKorisnika.Text);
                cmd.Parameters.AddWithValue("@ID", this.KorisnikId);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                MessageBox.Show("Korisnik uspesno izmenjen!", "Izmenjeno", MessageBoxButton.OK, MessageBoxImage.Information);
                UcitajKorisnike();
                Reset();

            }
            else
            {
                MessageBox.Show("Izaberite korisnika za izmenu!", "Izaberite", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            if(KorisnikId>0)
            {
                SqlCommand cmd = new SqlCommand("DELETE FROM Korisnik WHERE Id = @ID", con);
                cmd.CommandType = CommandType.Text;


                cmd.Parameters.AddWithValue("@ID", this.KorisnikId);

                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();

                MessageBox.Show("Korisnik uspesno izbrisan!", "Izbrisano", MessageBoxButton.OK, MessageBoxImage.Information);
                UcitajKorisnike();
                Reset();

            }
            else
            {
                MessageBox.Show("Izaberite korisnika za brisanje!", "Izaberite", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void Window_Load(object sender, RoutedEventArgs e)
        {
           
        }

        private void UcitajKorisnike()
        {
            SqlCommand cmd = new SqlCommand("Select * from Korisnik", con);
            DataTable dt = new DataTable();

            con.Open();
            SqlDataReader sdr = cmd.ExecuteReader();
            dt.Load(sdr);
            con.Close();

            DGKorisnici.ItemsSource= dt.DefaultView;

        }

        private void DGKorisnici_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DataGrid dg = sender as DataGrid;
            DataRowView dr = dg.SelectedItem as DataRowView;
            if (dr != null)
            {
                
                KorisnikId = Convert.ToInt32(dr["Id"]);
                
                tbIme.Text = dr["Ime"].ToString();
                tbPrezime.Text = dr["Prezime"].ToString();
                tbKorisnickoIme.Text = dr["KorisnickoIme"].ToString();
                tbLozinka.Text = dr["Lozinka"].ToString();
                tbEmail.Text = dr["Email"].ToString();
                cbTipKorisnika.Text = dr["TipKorisnika"].ToString();
            }
        }

      
        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
        
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SqlCommand cmd1 = new SqlCommand("Select * from Korisnik Where Ime='" + tbPretraga.Text + "'OR Prezime='" + tbPretraga.Text + "'OR KorisnickoIme='" + tbPretraga.Text + "'OR Email='" + tbPretraga.Text + "'OR TipKorisnika='" + tbPretraga.Text + "'", con);
            DataTable dt1 = new DataTable();

            con.Open();
            SqlDataReader sda = cmd1.ExecuteReader();
            dt1.Load(sda);
            con.Close();

            DGKorisnici.ItemsSource = dt1.DefaultView;
        }
    }
}
