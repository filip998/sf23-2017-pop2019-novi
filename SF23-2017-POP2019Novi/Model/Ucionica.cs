﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF23_2017_POP2019Novi.Model
{
    class Ucionica
    {
        private String SifraUcionice { get; set; }
        private String BrojUcionice { get; set; }
        private String BrojMesta { get; set; }
        public List<string> TipUcionice { get; set; }

        public Ucionica(string sifraUcionice, string brojUcionice, string brojMesta, List<string> tipUcionice)
        {
            SifraUcionice = sifraUcionice;
            BrojUcionice = brojUcionice;
            BrojMesta = brojMesta;
            TipUcionice = tipUcionice;
        }

    }
}
