﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF23_2017_POP2019Novi.Model
{
    class Ustanova
    {
        private String SifraUstanove { get; set; }
        private String Naziv { get; set; }
        private String Lokacija { get; set; }

        public Ustanova(string sifraUstanove, string naziv, string lokacija)
        {
            SifraUstanove = sifraUstanove;
            Naziv = naziv;
            Lokacija = lokacija;
        }
    }
}
