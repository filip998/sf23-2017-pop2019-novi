﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF23_2017_POP2019Novi.Model
{
    class Termin
    {
        private String SifraTermina { get; set; }
        private String VremeZauzeca { get; set; }
        private List<string> daniUNedelji = new List<string>();
        private String TipNastave { get; set; }
        private String Korisnik { get; set; }

        public Termin(string sifraTermina, string vremeZauzeca, List<string> daniUNedelji, string tipNastave, string korisnik)
        {
            SifraTermina = sifraTermina;
            VremeZauzeca = vremeZauzeca;
            this.daniUNedelji = daniUNedelji;
            TipNastave = tipNastave;
            Korisnik = korisnik;
        }
    }   
}
