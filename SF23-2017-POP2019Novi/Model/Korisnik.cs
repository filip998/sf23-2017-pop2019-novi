﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF23_2017_POP2019Novi.Model
{
    class Korisnik
    {

        private String Ime { get; set; }
        private String Prezime { get; set; }
        private String Email { get; set; }
        private String KorisnickoIme { get; set; }
        private String Lozinka { get; set; }
        private String TipKorisnika { get; set; }







        public Korisnik(string ime, string prezime, string email, string korisnickoIme, string lozinka, string tipKorisnika)
        {
            Ime = ime;
            Prezime = prezime;
            Email = email;
            KorisnickoIme = korisnickoIme;
            Lozinka = lozinka;
            TipKorisnika = tipKorisnika;



        }
    }
}
